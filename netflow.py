#!/usr/bin/python3

import socket
import sys
import time
import threading
import signal
import re
import ssl
from os import path, system
from http.server import HTTPServer, BaseHTTPRequestHandler, SimpleHTTPRequestHandler
from http.client import BadStatusLine
from urllib.parse import urlparse, parse_qsl
import requests
from requests.exceptions import ConnectionError
import warnings

VERSION='0.1'

def sigint_handler(signal, frame):
	print('exit')
	sys.exit(130)

def usage():
	global VERSION
	print(f"""\
{sys.argv[0]} {VERSION}

Usage: {sys.argv[0]} [command [DEST] ...]

    Where DEST is used, format it as
        'ip.addr.or.name:port'
    or
        'http[s]://fqdn[@ip.addr.or.name]:port'

    If DEST starts with http:// or https:// connect with
    that protocol instead of a plain TCP socket.

    E.g.
        127.0.0.1:8000
        localhost:8000
        http://fully.qualified.domain.name@127.0.0.1:8000
        https://fully.qualified.domain.name:443

Commands:
    via DEST        Forward the request through DEST.
                    Use this multiple times to send a chain of requests.

    ping DEST       Request a response from DEST. If the flow is allowed,
                    you will receive a 'Pong' message from DEST

    server DEST NAME
                    Daemonize this program with an identifying name. This
                    will make {sys.argv[0]} act as a server and accept
                    connections. DEST will listen on the specified interface
                    only. Use IP address 0.0.0.0 to listen on all 
                    interfaces. NAME will be sent in responses to clients.

    exit            Tell the listening server to exit

""")
	sys.exit()

def debug(msg, end=None):
	if False:
		if end:
			print(msg, end=end)
		else:
			print(msg)

def parse_address(uri):
	http_re = '(https?):\/\/([\.a-zA-Z0-9-]+)@?([0-9\.]+)?:?([0-9]+)?'
	m = re.match(http_re, uri)
	if m:
		return {
			'proto': m.group(1),
			'hostname': m.group(2),
			'ip': m.group(3) if m.group(3) else m.group(2),
			'port': int(m.group(4)) if m.group(4) else 80 if m.group(1) == 'http' else 443,
		}

	tcp_re = r'([a-zA-Z0-9\.-]+):([0-9]+)'
	m = re.match(tcp_re, uri)
	if m:
		return {
			'proto': 'tcp',
			'hostname': m.group(1),
			'ip': m.group(1),
			'port': int(m.group(2)),
		}
	return {
		'proto': 'unknown',
		'hostname': 'unknown',
		'ip': 'unknown',
		'port': 'unknown',
	}

def check_dest_param(args, ptr):
	try:
		p = parse_address(args[ptr+1])
		if p['proto'] == 'unknown':
			print('Destination argument is not properly formatted')
			usage()
	except IndexError as ie:
		print("'via' requires a destination argument")
		usage()

def parse_args():
	if '-v' in sys.argv or '-V' in sys.argv or '--version' in sys.argv or 'version' in sys.argv:
		print(f'{sys.argv[0]} {VERSION}')
		sys.exit()
	if '-h' in sys.argv or '--help' in sys.argv:
		usage()
	if len(sys.argv) < 2:
		print("Missing arguments")
		usage()
	
	ptr = 0
	args = sys.argv[1:]
	while ptr < len(args):
		if args[ptr] == 'via':
			check_dest_param(args, ptr)
		elif args[ptr] == 'server':
			check_dest_param(args, ptr)
			try:
				return(args[ptr], args[ptr+1], args[ptr+2])
			except IndexError as ie:
				print("Missing NAME parameter for 'server' command")
				usage()
		elif args[ptr] == 'ping':
			check_dest_param(args, ptr)
		elif args[ptr] == 'exit':
			check_dest_param(args, ptr)
		else:
			print(f"Invalid command '{args[ptr]}'")
			usage()
		ptr+=2
	return args

def tcp_client(cmd, connect, data):
	if not cmd:
		print("ERROR: no cmd at function call 'tcp_client'")
		sys.exit(1)

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	response = ''
	try:
		s.connect((connect['hostname'], connect['port']))
		debug(f'DATA: {data}')
		if data:
			if type(data[0]) is str:
				requestdata = ' '.join(data).encode()
			elif type(data[0]) is tuple:
				requestdata = ' '.join(f'{c} {d}' for c,d in data).encode()
			else:
				requestdata = ' '
		else:
			requestdata = ' '
		debug(f'Data: [{data}]\nRequestData: [{requestdata}]')
		if type(requestdata) is bytes:
			s.send(requestdata)
		else:
			s.send(requestdata.encode())
		debug(f'Data sent: {cmd} -> [{requestdata}]')
		response = s.recv(1024).decode()
		debug(response)
	except socket.error as e:
		print(f'Could not send data: {e}')
	s.close()
	return response

def http_client(cmd, connect, data):
	debug(f"Starting http_client with data: {data}")
	dest = data.pop(0)
	host = dest[1]
	query = (f'{c}={d}' for c,d in data)
	url = f'{connect["proto"]}://{connect["ip"]}:{connect["port"]}?{"&".join(query)}'
	debug(f'Connect: {connect}')
	debug(f'To: [{url}]')
	response_text = ''
	cert_msg = ''
	try:
		with warnings.catch_warnings(record=True) as warn:
			response = requests.get(url, headers={'Host': connect['hostname'], 'User-Agent': 'NetFlow'}, verify=False)
			response_text = response.text
			if response.headers.get('X-Server-Type', None) != 'NetFlow':
				response_text = f'Foreign HTML data from {host}'
				debug(response.text)
			if len(warn) == 1:
				if warn[0]._category_name == 'InsecureRequestWarning':
					cert_msg = ' - Warning: insecure request'
	except ConnectionError as ce:
		debug(f'Connection error: {ce}')
		response_text = f"ERROR: not an http reply from {host}\nDid you call this server with the correct protocol?"
	return response_text.replace('<<cert_msg>>', cert_msg)

def client(cmd='ping', dest='127.0.0.1:8000', args=[]):
	debug(f'cmd: {cmd}; dest: {dest}; args: {args}')

	p = parse_address(dest)
	response = ''
	cert_msg = ''
	debug(f'ARGS: {args}')
	if p['proto'] == 'tcp':
		response = tcp_client(cmd, p, args)
	elif p['proto'] in ('http', 'https'):
		data = [(cmd, dest)]
		while len(args) > 1:
			data.append((args.pop(0), args.pop(0)))
			debug(f'INTERMEDIATE DATA IS NOW: {data}')
		debug(f'DATA IS NOW: {data}')
		response = http_client(cmd, p, data)
	else:
		response = f'Unknown protocol: {p["proto"]}\n'

	return response

def server_thread(conn, addr, thread_id):
	global my_ip, my_name

	debug(f'[{thread_id}] Client {addr}:', end=' ')

	debug(f'[{thread_id}] Client connected: {addr[0]}:{addr[1]}')
	line = conn.recv(1024).decode()
	debug(f'[{thread_id}] Received: {line}')
	words = line.split(' ')
	debug(f'Recieved words: {words}')

	if len(words) > 1:
		if words[1] == my_ip:
			words = words[2:]

	request = ' '.join(words)
	if request == ' ':
		request = 'ping'

	print(f'Client: {addr[0]}:{addr[1]}; Request: {request}')

	if not words:
		words = ['pong', '']
	
	if words[0] == 'exit':
		print('exit')
		conn.send(f'Server {name}[{thread_id}] exitting'.encode())
		time.sleep(1)
		sys.exit()
	elif words[0] == 'via' or words[0] == 'ping':
		debug(f"[{thread_id}] Processing 'via'; words: {words}")
		cmd = words.pop(0)
		dest = words.pop(0)
		debug(f'via -> {dest}')
		data = client(cmd, dest, words)
		response = f'Server {my_name}[{thread_id}] ({my_ip}) forwarded to {dest}\n{data}'
		response = f'via {my_name}[{thread_id}] ({my_ip})\n{data}'
		conn.send(response.encode())
	else:
		debug('ping -> pong')
		conn.send(f'Pong from {my_name}[{thread_id}] ({my_ip})\n'.encode())
	conn.close()

def tcp_server(connect, name):
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			s.bind((connect['ip'], connect['port']))
		except socket.error as e:
			print(f'Bind failed for {connect["ip"]}:{connect["port"]}: {e}')
			sys.exit(1)
		
		s.listen(10)
		print(f"Server '{name}' now listening on {connect['ip']}:{connect['port']}\nPress Ctrl-C to exit...")
		
		thread_counter = 0
		while True:
			conn, addr = s.accept()
			thread_counter += 1
			thread = threading.Thread(target=server_thread, args=(conn, addr, thread_counter))
			thread.start()
	
		print('Closing main socket')
		s.close()

class RequestHandler(SimpleHTTPRequestHandler):	# , request, client_address, server):
	def do_GET(self):
		global my_name, my_ip
		debug(f"Processing '{self.path}' for {self.client_address}")
		debug(f"url parts: {parse_qsl(urlparse(self.path).query)}")
		args = parse_qsl(urlparse(self.path).query)
		cert_tmpl = ''
		user_agent = ''
		if self.headers.get('User-Agent', 'nope') == 'NetFlow':
			cert_tmpl = '<<cert_msg>>'
			user_agent = 'NetFlow'
		x_forwarded_for = self.headers.get('X-Forwarded-For', '')
		if x_forwarded_for:
			x_forwarded_for = f' for {x_forwarded_for}'

		if args:
			cmd = args[0][0]
		else:
			cmd = 'send_pong'

		if cmd == 'ping' or cmd == 'via':
			response_text = client(cmd, args[0][1], args)
			if user_agent != 'NetFlow':
				response_text = '<Foreign HTML data>'
			response_body = f'via {my_name} ({my_ip}{cert_tmpl}){x_forwarded_for}\n{response_text}'
		else:
			response_body = f'Pong from {my_name} ({my_ip}{cert_tmpl}){x_forwarded_for}\n'
			
		self.send_response(200, "OK")
		self.send_header('X-Server-Type', 'NetFlow')
		self.end_headers()
		self.wfile.write(response_body.encode())


def http_server(connect, name):
	try:
		httpd = HTTPServer((connect['ip'], connect['port']), RequestHandler)
	except Exception as e:
		print(f'Bind failed for {connect["proto"]}://{connect["hostname"]}:{connect["port"]} (@{connect["ip"]}): {e}')
		sys.exit()

	if connect['proto'] == 'https':
		if not (path.exists('key.pem') and path.exists('cert.pem')):
			system('openssl req -nodes -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 365 -subj "/C=NL/ST=Zuid-Holland/L=Den Haag/O=MinBZK/OU=Logius/CN=NetFlow"')

		context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
		context.load_cert_chain('cert.pem', 'key.pem')
		httpd.socket = context.wrap_socket(httpd.socket, server_side=True)
	print(f"Server '{name}' now listening on {connect['proto']}://{connect['hostname']}:{connect['port']} (@{connect['ip']})")
	print("Press Ctrl-C to exit...")
	httpd.serve_forever()

def server(dest, name='CheckNetFlow'):
	p = parse_address(dest)

	if p['proto'] == 'tcp':
		tcp_server(p, name)
	elif p['proto'] in ('http', 'https'):
		http_server(p, name)
	else:
		print(f"Should be handling '{p['proto']} request")

signal.signal(signal.SIGINT, sigint_handler)

args = parse_args()
if 'server' in args:
	my_ip = args[1]
	my_name = args[2]
	server(my_ip, my_name)
else:
	cmd = args.pop(0)
	dest = args.pop(0)
	debug(f"Calling server {dest} with {cmd}")
	response = client(cmd, dest, args)
	print(response)
