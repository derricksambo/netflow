# NetFlow

NetFlow allows a "ping" to be sent through a chain of servers. This way an infrastructure-setup can be tested easily without having to setup all servers before hand.

The tool supports TCP sockets and the HTTP protocol as well. HTTPS is also supported where you can use your own certificate (provide it as `cert.pem` with the key in `key.pem` located in the same directory as the tool) or let the tool use a self-signed certificate. It will be created automatically when you start the tool in 'daemon' mode if either of the aforementioned files are missing.

## Use Cases
The tool can be used to test different infra structure needs.

It will also work for proxy servers as it will add a 'forwarded' message in the output when it encounters a 'X-Forwarded-For' header.

It can also be used for example to test load-balancers by starting multiple servers behind a load-balancer and see if e.g. a round-robin method is used or when a server becomes unrepsonsive. Currently pinning to one server based on a session will not work as sessions are not supported over multiple invocations of the script.

It might also be used to check a fail-over scenario that is handled by the infra structure (and not by the application).

## Requirements:
This tool requires the python package 'requests'

Install with:
    
    pip install requests
    
    
## Usage
    ./netflow.py [command [DEST] ...]

    Where DEST is used, format it as
        'ip.addr.or.name:port'
    or
        'http[s]://fqdn[@ip.addr.or.name]:port'

    If DEST starts with http:// or https:// connect with
    that protocol instead of a plain TCP socket.

    E.g. for TCP sockets:
        127.0.0.1:8000
        localhost:8000
    
        for HTTP(S) (use first format when hostname doesn't resolve to the correct IP)
        http://fully.qualified.domain.name@127.0.0.1:8000
        https://fully.qualified.domain.name:443

    Commands:
        via DEST        Forward the request through DEST.
                        Use this multiple times to send a chain of requests.

        ping DEST       Request a response from DEST. If the flow is allowed,
                        you will receive a 'Pong' message from DEST

        server DEST NAME
                        Daemonize this program with an identifying name. This
                        will make ./netflow.py act as a server and accept
                        connections. DEST will listen on the specified interface
                        only. Use IP address 0.0.0.0 to listen on all
                        interfaces. NAME will be sent in responses to clients.

        exit            Tell the listening server to exit

## Example
Start 3 servers (in this case on the same machine with different ports):

    ./netflow.py server https://127.0.0.1:8001 WebServer &
    ./netflow.py server 127.0.0.1:8011 AppServer &
    ./netflow.py server 127.0.0.1:8021 DbServer &


Check the data flow from client via WebServer via AppServer all the way to the DbServer:

    ./netflow.py via https://127.0.0.1:8001 via 127.0.0.1:8011 ping 127.0.0.1:8021


Client output:

    via WebServer (https://127.0.0.1:8001 - Warning: insecure request)
    via AppServer[1] (127.0.0.1:8011)
    Pong from DbServer[1] (127.0.0.1:8021)

The "insecure request" warning is issued because the server uses a self signed certificate.
The numbers in square brackets are the thread count on the TCP server. This way loops can also be detected (or created).

