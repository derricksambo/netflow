# To Do

- **Redirects** <br>
  When a HTTP client receives a redirect, report that back to the caller including all the URL's it has passed up to the final one.
