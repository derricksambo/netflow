#!/usr/bin/bash

LISTEN=$1
NAME=$2
REQUIREMENTS="requests"

usage () {
	echo "\t
	Usage: $0 <LISTEN_ADDRESS> <NAME>

	where
	LISTEN_ADDRESS is in the format '[http[s]://[fqdn.name@]]ip.addr.or.name:port'
	NAME identifies this service easily
"
	exit
}

if [ -z "$NAME" ]
then
	echo "\tMissing parameter(s)"
	usage
fi

PROTO=$(echo "$LISTEN" | sed 's^://.*$^^')
if [ "$PROTO" == "$LISTEN" ]
then
	PROTO=""
else
	PROTO="${PROTO}://"
fi

IP=$(echo "$LISTEN" | sed -E 's/^(.*[@\/])?([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+).*/\2/g')
echo "IP: $IP"

PORT=$(echo "$LISTEN" | sed 's/.*://' | grep -E "^[0-9]+$")
if [ -z "$PORT" ]
then
	echo "\tMissing port number for LISTEN_ADDRESS"
	usage
fi
	

echo "Port mapping: $LISTEN"
docker run -d -it --rm --name ${NAME}_$(echo $LISTEN|sed 's^://^_^' | sed 's/[\@:\/]/_/g') -p $IP:$PORT:$PORT -v "$(pwd)":/usr/src/myapp -w /usr/src/myapp python:alpine /bin/sh -c "pip install $REQUIREMENTS ; python netflow.py server ${PROTO}0.0.0.0:${PORT} $NAME"
